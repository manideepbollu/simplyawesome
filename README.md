# Simple Awesome: An extended ERP project for restaurant businesses

We have just started with the project; we will update this section with more details as the project 
continues to move further.

Things we may cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ....


Coding is being done by [Manideep Bollu](mailto:manideep@msn.com)